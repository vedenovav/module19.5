﻿// Module19.5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

class Animal {
    
public:
    void virtual Voice() {
        std::cout << "animal voice\n";
    };
    ~Animal() {};
};

class Dog : public Animal {
    
public:
    void Voice() override {
        std::cout << "Woof!\n";
    };
};

class Cat : public Animal {
public:
    void Voice() override {
        std::cout << "Meewow!\n";
    };
};

class Duck : public Animal {
public:
    void Voice() override {
        std::cout << "Krya!\n";
    };
};

int main()
{
    Animal **ptr = new Animal *[3];
    ptr[0] = new Dog;
    ptr[1] = new Cat;
    ptr[2] = new Duck;
        for (int i = 0; i < 3; i++) {
            ptr[i]->Voice();
        };
        for (int i = 0; i < 3; i++) {
            delete ptr[i];
        };

    delete[] ptr;
    return 0;
   

   
}
// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
